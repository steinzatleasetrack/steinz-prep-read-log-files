import os
import json
import constants
from models.date_filter import DateFilter

class ResponseReader:
    def __init__(self, storage, request):
        self.storage = storage
        self.request = request

    def read_responses(self):
        responses = {}
        directory = os.fsencode(self.storage)
        for file in os.listdir(directory):
            filename = os.fsdecode(file)
            date_filter = DateFilter(self.request)
            if not date_filter.is_file_between_dates(filename):
                continue
            with open(os.path.join(self.storage, filename)) as log_file:
                json_found = False
                request_found = False
                request_complete = False
                json_filename = filename.replace(constants.LOG_EXT, constants.JSON_EXT)
                json_str = ""
                output = open(os.path.join("responses", json_filename), "w")
                for line in log_file:
                    if request_complete:
                        continue
                    if (
                        line.startswith("{")
                        and not request_found
                    ):
                        request_found = True
                    elif (
                        line.startswith("{")
                        and request_found
                        and not json_found
                    ):
                        json_found = True
                        json_str += line
                        output.write(line)
                    elif (
                        line.startswith("}")
                        and request_found
                        and json_found
                    ):
                        json_found = False
                        request_complete = True
                        json_str += line
                        json_obj = json.loads(json_str)
                        if json_obj["data"] is not None:
                            responses[json_obj["data"]["id"]] = json_obj["data"]
                        output.write(line)
                    elif (
                        json_found
                        and request_found
                    ):
                        json_str += line
                        output.write(line)
                output.close()
        return responses