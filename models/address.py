class Address:
    def __init__(self, object):
        self.object = object

    def __str__(self):
        result = ""
        if self.object["street"] is not None:
            result += self.object["street"] + ", "
        if self.object["city"] is not None:
            result += self.object["city"] + ", "
        if self.object["state"] is not None:
            result += self.object["state"] + ", "
        if self.object["zip"] is not None:
            result += self.object["zip"]
        return result