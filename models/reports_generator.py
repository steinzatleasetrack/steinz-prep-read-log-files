import os
import csv
import json
import constants
from models.address import Address

class ReportsGenerator:
    def __init__(self, request, responses):
        self.start = request["start"]
        self.end = request["end"]
        self.responses = responses

    def run_reports(self):
        directory = os.fsencode("csv_files")
        ocr_author_id = constants.OCR_PREP_AUTHOR_ID
        fields = [
            'Start Date',
            'End Date',
            'Visitor ID',
            'Unit ID',
            'Author',
            'Carrier',
            'Document Type',
            'First Name',
            'Last Name',
            'Address',
            'Policy Number',
            'Premium',
            'Liability Limit',
            'Effective Date',
            'Expiration Date',
            'Mailing Address',
            'Additional Interest'
        ]
        for file in os.listdir(directory):
            filename = os.fsdecode(file)
            with open(os.path.join(constants.CSV_FOLDER, filename)) as csv_file:
                is_invalid = "invalid" in filename
                reader = csv.reader(csv_file, delimiter=',', quotechar='"')
                with open(os.path.join("output_files", filename), 'w', newline='') as output_file:
                    writer = csv.writer(output_file)
                    writer.writerow(fields)
                    previous_row_id = 0
                    for row in reader:
                        author_id = row[3]
                        data_str = row[5]
                        output_row = [
                            None,
                            None,
                            None,
                            None,
                            None,
                            None,
                            None,
                            None,
                            None,
                            None,
                            None,
                            None,
                            None,
                            None,
                            None,
                            None,
                            None
                        ]
                        if data_str != "data":
                            data = json.loads(data_str)
                            output_row[0] = self.start
                            output_row[1] = self.end
                            if 'id' in data:
                                output_row[2] = data['id']
                            if 'unit' in data:
                                output_row[3] = data['unit']
                            if author_id == ocr_author_id:
                                output_row[4] = "OCR"
                            else:
                                output_row[4] = "Human"
                            if 'carrier' in data:
                                output_row[5] = data['carrier']
                            if 'type' in data:
                                output_row[6] = data['type']
                            if 'firstName' in data:
                                output_row[7] = data['firstName']
                            if 'lastName' in data:
                                output_row[8] = data['lastName']
                            if 'address' in data:
                                output_row[9] = data['address']
                            if 'policyNumber' in data:
                                output_row[10] = data['policyNumber']
                            if 'premium' in data:
                                output_row[11] = data['premium']
                            if 'liabilityLimit' in data:
                                output_row[12] = data['liabilityLimit']
                            if 'effDate' in data:
                                output_row[13] = data['effDate']
                            if 'expDate' in data:
                                output_row[14] = data['expDate']
                            if 'mailingAddress' in data:
                                output_row[15] = data['mailingAddress']
                            if 'additionalInterest' in data:
                                output_row[16] = data['additionalInterest']
                            writer.writerow(output_row)
                            if (
                                is_invalid
                                and previous_row_id != data["id"]
                                and data['id'] in self.responses
                            ):
                                output_row = [
                                    None,
                                    None,
                                    None,
                                    None,
                                    None,
                                    None,
                                    None,
                                    None,
                                    None,
                                    None,
                                    None,
                                    None,
                                    None,
                                    None,
                                    None,
                                    None,
                                    None
                                ]
                                response = self.responses[data['id']]
                                output_row[0] = self.start
                                output_row[1] = self.end
                                output_row[2] = response['id']
                                output_row[3] = response['unit']
                                output_row[4] = "OCR"
                                output_row[5] = response['carrier']
                                output_row[6] = response['type']
                                output_row[7] = response['firstName']
                                output_row[8] = response['lastName']
                                output_row[9] = str(Address(response['address']))
                                output_row[10] = response['policyNumber']
                                output_row[11] = response['premium']
                                output_row[12] = response['liabilityLimit']
                                output_row[13] = response['effDate']
                                output_row[14] = response['expDate']
                                output_row[15] = str(Address(response['mailingAddress']))
                                output_row[16] = response['additionalInterest']
                                writer.writerow(output_row)
                            previous_row_id = data['id']