import os
import json
from re import L
import constants
from models.date_filter import DateFilter

class RequestReader:
    def __init__(self, storage, request):
        self.storage = storage
        self.request = request
    
    def read_requests(self):
        requests = {}
        directory = os.fsencode(self.storage)
        for file in os.listdir(directory):
            filename = os.fsdecode(file)
            date_filter = DateFilter(self.request)
            if not date_filter.is_file_between_dates(filename):
                continue
            with open(os.path.join(self.storage, filename)) as log_file:
                json_found = False
                request_complete = False
                json_filename = filename.replace(constants.LOG_EXT, constants.JSON_EXT)
                json_str = ""
                output = open(os.path.join("requests", json_filename), "w")
                for line in log_file:
                    if request_complete:
                        continue
                    if (
                        line.startswith("{")
                        and not json_found
                    ):
                        json_found = True
                        output.write(line)
                        json_str += line
                    elif (
                        line.startswith("}")
                        and json_found
                    ):
                        json_found = False
                        request_complete = True
                        output.write(line)
                        json_str += line
                        json_obj = json.loads(json_str)
                        requests[json_obj["id"]] = json_obj
                    elif json_found:
                        output.write(line)
                        json_str += line
                output.close()
        return requests
