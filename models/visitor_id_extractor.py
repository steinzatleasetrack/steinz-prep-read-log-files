import os
import constants
from models.date_filter import DateFilter

class VisitorIdExtractor:
    def __init__(self, storage, request):
        self.storage = storage
        self.request = request
        self.directory = os.fsencode(self.storage)

    def get_all_visitor_ids(self):
        visitor_ids = []
        output = open(os.path.join(constants.VISITOR_IDS_FOLDER, "all_visitor_ids.txt"), "w")
        for file in os.listdir(self.directory):
            filename = os.fsdecode(file)
            date_filter = DateFilter(self.request)
            if not date_filter.is_file_between_dates(filename):
                continue
            with open(os.path.join(self.storage, filename)) as log_file:
                for line in log_file:
                    if line.startswith("    \"id\": "):
                        visitor_id = line[10:]
                        output.write(visitor_id)
                        visitor_ids.append(visitor_id.replace(",\n",""))
                        break
        output.close()
        return visitor_ids

    def get_unsupported_visitor_ids(self):
        visitor_ids = []
        output = open(os.path.join(constants.VISITOR_IDS_FOLDER, "unsupported_visitor_ids.txt"), "w")
        for file in os.listdir(self.directory):
            filename = os.fsdecode(file)
            date_filter = DateFilter(self.request)
            if not date_filter.is_file_between_dates(filename):
                continue
            with open(os.path.join(self.storage, filename)) as log_file:
                for line in log_file:
                    if line.startswith("    \"id\": "):
                        visitor_id = line[10:]
                    if line == "Carrier and Type combination is not supported\n":
                        output.write(visitor_id)
                        visitor_ids.append(visitor_id.replace(",\n",""))
        output.close()
        return visitor_ids