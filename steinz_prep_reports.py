import flask
from flask import request
from models.settings_builder import SettingsBuilder
from models.request_reader import RequestReader
from models.response_reader import ResponseReader
from models.visitor_id_extractor import VisitorIdExtractor
from models.reports_generator import ReportsGenerator

app = flask.Flask(__name__)
settings_builder = SettingsBuilder()

@app.route("/")
def index():
  return "200"

@app.route('/api/steinz_prep_reports', methods=['POST'])
def run_reports():
    # request_data = { "start": "12/31/2021", "end": "01/31/2022" }
    request_data = request.get_json()
    
    request_reader = RequestReader(settings_builder.storage, request_data)
    requests = request_reader.read_requests()

    response_reader = ResponseReader(settings_builder.storage, request_data)
    responses = response_reader.read_responses()

    visitor_id_extractor = VisitorIdExtractor(settings_builder.storage, request_data)
    all_visitor_ids = visitor_id_extractor.get_all_visitor_ids()
    unsupported_visitor_ids = visitor_id_extractor.get_unsupported_visitor_ids()

    reports_generator = ReportsGenerator(request_data, responses)
    reports_generator.run_reports()
    return "Finished"

app.run(settings_builder.host, settings_builder.port, settings_builder.debug)